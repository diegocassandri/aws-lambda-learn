'use strict';

module.exports.test = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go SeniorX',
        input: event,
      },
      null,
      2
    ),
  };
};

module.exports.testpost = async (event,context) => {

  let _parsed;
  try{
    _parsed = JSON.parse(event.body);

    _parsed.teste = _parsed.teste + ' Abharchely';

  } catch(err) {
    console.error(`Could not parse requested JSON ${event.body}: ${err.stack}`);
    return {
      statusCode: 500,
      error: `Could not parse requested JSON: ${err.stack}`
    };
  }

  return {
    statusCode: 200,
    body: JSON.stringify(_parsed),
  };
};
